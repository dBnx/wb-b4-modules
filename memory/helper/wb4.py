import random
from cocotb.triggers import RisingEdge, FallingEdge, Timer


def random_bits(n: int) -> int:
    return random.randint(0, 2**n-1)


async def reset(dut):
    await FallingEdge(dut.wb_clk)
    dut.wb_rst.value = 1
    dut.wb_cyc.value = 0
    dut.wb_stb.value = 0
    dut.wb_we.value = 0
    await RisingEdge(dut.wb_clk)
    await RisingEdge(dut.wb_clk)
    dut.wb_rst.value = 0
    assert dut.wb_stall == 0, "Reset DUT shall not stall"
    assert dut.wb_stall == 0, "Reset DUT shall not ack if not selected"


async def pipeline_read(dut, addr: int) -> int:
    # TODO:
    # - Respect wb_stall

    dut.wb_stb.value = 1
    dut.wb_we.value = 0
    dut.wb_adr.value = addr
    dut.wb_dat_i.value = random_bits(n=32)

    await RisingEdge(dut.wb_clk)
    await Timer(1, units="ns")
    dut.wb_adr.value = random_bits(n=12)
    result = dut.wb_dat_o.value

    dut.wb_stb.value = 0
    dut.wb_we.value = 0
    return result


async def pipeline_read_n(dut, addr: int, n: int) -> list[int]:
    pass


async def pipeline_write(dut, addr: int, data: int):
    assert data & 0xFFFF_FFFF == data, "Data must be 32b"
    dut.wb_stb.value = 1
    dut.wb_we.value = 1
    dut.wb_adr.value = addr
    dut.wb_dat_i.value = data

    await RisingEdge(dut.wb_clk)
    dut.wb_adr.value = random_bits(n=12)
    dut.wb_dat_i.value = random_bits(n=32)

    dut.wb_stb.value = 0
    dut.wb_we.value = 0


async def pipeline_write_n(dut, addr: int, data: list[int]):
    pass
