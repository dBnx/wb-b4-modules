import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, FallingEdge, Timer
from cocotb.result import TestFailure
import random


async def reset_dut(dut):
    dut.rst <= 1
    await FallingEdge(dut.clk)
    await RisingEdge(dut.clk)
    await FallingEdge(dut.clk)
    dut.rst <= 0


def assert_equal_bytes(be: int, expected: int, value: int, errmsg="") -> None:
    for i in range(3):
        byte_enabled = be & (1 << i)
        if not byte_enabled:
            continue

        exp = (expected >> 8*i) & 0xFF
        val = (value >> 8*i) & 0xFF
        assert hex(exp) == hex(val), f"{errmsg}: Mismatch in byte {i}"


@cocotb.test()
async def test_ram_module(dut):
    cocotb.start_soon(Clock(dut.clk, 10, units="ns").start())

    await reset_dut(dut)

    for _ in range(20):
        # Generate random address, data, and byte enable values
        address = random.randint(0, 2**dut.ADDR_WIDTH.value - 1)
        data = random.randint(0, 2**dut.DATA_WIDTH.value - 1)
        byte_enable = random.randint(0, 15)

        # Write
        dut.adr <= address
        dut.dat_i <= data
        dut.be_i <= byte_enable
        dut.we_i <= 1
        dut.stb_i <= 1
        dut.cyc_i <= 1
        await RisingEdge(dut.clk)
        await FallingEdge(dut.clk)
        assert dut.ack.value == 1, "Write operation not acknowledged"
        dut.stb_i <= 0

        # Read
        dut.adr <= address
        dut.we_i <= 0
        dut.stb_i <= 1
        dut.cyc_i <= 1
        await RisingEdge(dut.clk)
        await FallingEdge(dut.clk)
        assert dut.ack.value == 1, "Read operation not acknowledged"
        dut.stb_i <= 0

        data_out = dut.dat_o.value.integer

        assert_equal_bytes(byte_enable, data, data_out,
                           f"Data mismatch @ {hex(address)} {hex(data)} != {hex(data_out)} BE: {bin(byte_enable)}")

    address = random.randint(0, (1 << dut.ADDR_WIDTH.value) - 1)

    dut.adr <= address
    dut.we_i <= 0
    dut.stb_i <= 1
    dut.cyc_i <= 1
    await RisingEdge(dut.clk)
    await FallingEdge(dut.clk)
    assert dut.ack.value == 1, "Read operation not acknowledged"
    dut.stb_i <= 0


@cocotb.test()
async def test_ram_edge_cases(dut):
    clock = Clock(dut.clk, 10, units="ns")  # Create a 10ns period clock
    cocotb.fork(clock.start())  # Start the clock

    await FallingEdge(dut.clk)  # Synchronize with the clock edge

    # Reset the module
    await reset_dut(dut)

    # Test edge cases for address boundaries and byte enables
    for address in [0, (1 << dut.ADDR_WIDTH.value) - 1]:
        for data in [0xFFFFFFFF, 0x00000000]:
            # Write
            dut.adr <= address
            dut.dat_i <= data
            dut.be_i <= 0xF
            dut.we_i <= 1
            dut.stb_i <= 1
            dut.cyc_i <= 1
            await RisingEdge(dut.clk)
            await FallingEdge(dut.clk)
            dut.stb_i <= 0

            # Read
            dut.adr <= address
            dut.we_i <= 0
            dut.stb_i <= 1
            dut.cyc_i <= 1
            await RisingEdge(dut.clk)
            await FallingEdge(dut.clk)
            dut.stb_i <= 0

            data_out = dut.dat_o.value
            assert_equal_bytes(0xF, data, data_out,
                               f"Data mismatch @ {hex(address)} {hex(data)} != {hex(data_out)} BE: {bin(0xF)}")

    # For cleaner trace
    await RisingEdge(dut.clk)


def test_runner():
    import os
    from pathlib import Path
    from cocotb.runner import get_runner

    hdl_toplevel = "wb4p_ram"
    sim = os.getenv("SIM", "verilator")
    project_path = Path(__file__).resolve().parent

    verilog_sources = [
        project_path / f"{hdl_toplevel}.sv",
    ]

    build_args = ["--trace", "--trace-structs"] if sim == "verilator" else []
    runner = get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
        vhdl_sources=[],
        hdl_toplevel=hdl_toplevel,
        always=True,
        build_args=build_args,
        build_dir=f"build/{hdl_toplevel}",
    )

    runner.test(hdl_toplevel=hdl_toplevel, test_module=f"{hdl_toplevel}_test,",
                waves=True, extra_env={"WAVES": "1"})


if __name__ == "__main__":
    test_runner()
