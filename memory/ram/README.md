<!--toc:start-->
- [Datasheets](#datasheets)
  - [wb4p_ram](#wb4pram)
<!--toc:end-->

# Datasheets


## wb4p_ram

| Description                    | Specification |
| ------------------------------ | ------------- |
| General description            | Simple RAM module |
| Supported cycles               | SLAVE, PIPELINE READ/WRITE |
| Data port - size (granularity) | 32b (8b) |
| Data transfer ordering         | Little Endian |
| Clock frequency constraints    | NONE (determined by memory primitive) |
| Supported signals list         | See interface below |
| Special requirements           | Assumes use of synchronous RAM primitive with asynchronous read capability |


## Interface

```verilog
module wb4p_ram #(
    parameter int ADDR_WIDTH = 10,
    parameter int DATA_WIDTH = 32,
    parameter FILENAME = "" // Initialize memory with given intel .hex file
)(
    // <<< System >>>
    input  logic                  clk,
    input  logic                  rst,
    // Wishbone 4b pipelined peripheral
    input  logic                  we_i,
    input  logic                  stb_i,
    input  logic                  cyc_i,
    output logic                  ack,
    input  logic [           3:0] be_i,
    input  logic [ADDR_WIDTH-1:0] adr, // has two more bits for byte-wise addressing
    input  logic [DATA_WIDTH-1:0] dat_i,
    output logic [DATA_WIDTH-1:0] dat_o
);
```


## Signals

Alternating writes and reads after a reset:

![wb4p_ram.sv](/uploads/8ee37f90c250b8354ebc31fd505238d8/image.png)
