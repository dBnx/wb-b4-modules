module wb4p_ram #(
    parameter int ADDR_WIDTH = 10,
    parameter int DATA_WIDTH = 32, // FIXME: Not yet supported!
    parameter FILENAME = "" // Initialize memory with given intel .hex file
)(
    // <<< System >>>
    input  logic                  clk,
    input  logic                  rst,
    // Wishbone 4b pipelined peripheral
    input  logic                  we_i,
    input  logic                  stb_i,
    input  logic                  cyc_i,
    output logic                  ack,
    input  logic [           3:0] be_i,
    input  logic [ADDR_WIDTH-1:0] adr, // has two more bits for byte-wise addressing
    input  logic [DATA_WIDTH-1:0] dat_i,
    output logic [DATA_WIDTH-1:0] dat_o
);
  // ╭───────────────────────────────────────────────────────────────────────╮
  // │                               Parameters                              │
  // ╰───────────────────────────────────────────────────────────────────────╯
  localparam int DEPTH = 1 << ADDR_WIDTH;

  // ╭───────────────────────────────────────────────────────────────────────╮
  // │                                 Types                                 │
  // ╰───────────────────────────────────────────────────────────────────────╯
  // State Machine
  typedef enum logic [2:0] {
      IDLE  = 3'h0,
      READ  = 3'h1,
      WRITE = 3'h2
  } state_t;


  // ╭───────────────────────────────────────────────────────────────────────╮
  // │                                Signals                                │
  // ╰───────────────────────────────────────────────────────────────────────╯
  logic [DATA_WIDTH-1:0] ram [DEPTH];

  logic [ADDR_WIDTH-1:0] adr_reg; // Points to a DWord
  logic [DATA_WIDTH-1:0] dat_o_reg;
  state_t                state;


  always_ff @(posedge clk) begin
    if(rst) begin
      ack <= 0;
    end else begin
      ack <= stb_i;
    end
  end

  // ╭───────────────────────────────────────────────────────────────────────╮
  // │                              Assignments                              │
  // ╰───────────────────────────────────────────────────────────────────────╯
  initial begin
      if(FILENAME != "") begin
          $readmemh(FILENAME, ram);
      end
  end


  // ╭───────────────────────────────────────────────────────────────────────╮
  // │                                 Logic                                 │
  // ╰───────────────────────────────────────────────────────────────────────╯
  always_comb begin
      if (state == IDLE)
          dat_o = dat_o_reg;
      else
          dat_o = ram[adr_reg];
  end

  always_ff @(posedge clk or posedge rst) begin
      if (rst) begin
          state <= IDLE;
          adr_reg <= 0;
          dat_o_reg <= 0;
      end else begin
          case(state)
              IDLE: begin
                  if (cyc_i && stb_i) begin
                      adr_reg <= adr;
                      state <= we_i ? WRITE : READ;
                  end
              end
              READ: begin
                  if (!cyc_i || !stb_i) begin
                      state <= IDLE;
                  end else begin
                      dat_o_reg <= ram[adr_reg];
                      state <= !stb_i ? IDLE : we_i ?  WRITE : READ;
                  end
              end
              WRITE: begin
                  if (!cyc_i || !stb_i) begin
                      state <= IDLE;
                  end else begin
                      // Apply byte enables
                      if (be_i == 4'b1111) begin
                          ram[adr_reg] <= dat_i;
                      end else begin
                          if (be_i[0]) ram[adr_reg][7:0] <= dat_i[7:0];
                          if (be_i[1]) ram[adr_reg][15:8] <= dat_i[15:8];
                          if (be_i[2]) ram[adr_reg][23:16] <= dat_i[23:16];
                          if (be_i[3]) ram[adr_reg][31:24] <= dat_i[31:24];
                      end
                      state <= !stb_i ? IDLE : we_i ? WRITE : READ;
                  end
              end
              default: begin
                state <= IDLE;
              end
          endcase
      end
  end

endmodule

