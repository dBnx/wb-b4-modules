# Wishbone B4 module collection

## Naming scheme

```
wbV[m][c|p]_class[[_details]]
```

- `V` Wishbone version. In this case 4 for the Wishbone B4 specification
- `m` If present, indicates that is a wishbone controller (master) or the absence means it's a wishbone device.
- `c|p` Supported cycle type  
    - `c` classic cycles 
    - `p` pipeline cycles
    - not present: classic and pipeline cycles are supported. Thus a `CTI` tag can be found in the interface.
- `class`   What kind of device is it?
- `details` Additional informations are appended afterwards
- `[_details]` Additional informations are appended afterwards


### Examples

- `wb4p_uart_buffered` - Wishbone UART device supporting only pipeline mode with integrated buffer
- `wb4m_uart` - Wishbone UART master

## How to install

- Add this library and all dependencies to your git-based project as a submodule via:
```sh
git submodule add https://gitlab.com/dBnx/wb-b4-modules external/wb_b4_modules
git submodule add https://gitlab.com/dBnx/primitives external/primitives 
git submodule update --init --recursive
```
- Add all SystemVerilog sources to your project and commit `.gitmodules`, if it didn't exist yet.


## Run tests


### Install dependencies

- Install Python 3, venv and `verilator` (or another [cocotb](https://github.com/cocotb/cocotb) backend)
  - Via pacman on Arch-based linux distributions:
```sh
sudo pacman -S python verilator
```
- (Optional, recommended) Create a virtual environment 
```sh
python -m venv venv
```
- Install python requirements
  - Via pip
```sh
python -m pip install -r requirements.txt
```
  - Via pacman on Arch-based linux distributions:
```sh
sudo pacman -S python-cocotb python-pytest python-pytest-xdist python-pytest-cov
```

`pytest-xdist` is not strictly necessary, but recommended to speed it up by a huge margin and thus added to requirements.txt


### Dendencies

- Without `pytest-xdist`:
```sh
pytest --last-failed
```
- With `pytest-xdist`:
```sh
pytest -n auto --dist worksteal --last-failed
```
Intstead of auto a custom number of parallel processes can be used.


## About the structure of the project

Ever module `module` has an associated 
- `module.sv` file with the implementation
- `module_test.py` containing cocotb unit tests
- `module.sav` that contains an (optional) GTKwave layout for easy inspection of trace dumps

Those files can be found in the appropriate subfolder. All of the contained modules may depend on any other module. This
is (for now - due to rapid changes) not documented and a user of this library may assume total interdependence. Meaning
all files should be added in a project to guarantee succesfull compilation.

Contained modules may also depend on other libraries, those are mentioned in the _How to install_ section above.
