interface wishbone_b4_if (
    input wb_clk,
    input wb_rst
);
  // Busses
  parameter int DATA_GRANULARITY = 8;
  parameter int DATA_WIDTH = 8;
  parameter int ADDR_WIDTH = 8;
  // Tags
  parameter int DATA_TAG_WIDTH = 0;
  parameter int ADDR_TAG_WIDTH = 0;
  parameter int CYCLE_TAG_WIDTH = 0;

  localparam int SelectWidth = DATA_WIDTH / DATA_GRANULARITY;

  // Controls
  logic                       we;
  logic                       stb;
  logic                       cyc;
  logic                       stall;
  logic                       lock;
  // Termiators
  logic                       ack;
  logic                       err;
  logic                       rty;
  // Buses
  logic [     ADDR_WIDTH-1:0] adr;
  logic [    SelectWidth-1:0] sel;
  logic [     DATA_WIDTH-1:0] dat_m2s;
  logic [     DATA_WIDTH-1:0] dat_s2m;
  // Tags
  logic [ ADDR_TAG_WIDTH-1:0] tga;
  logic [ DATA_TAG_WIDTH-1:0] tgd;
  logic [CYCLE_TAG_WIDTH-1:0] tgc;

  modport controller(
      // Syscon
      input clk, rst,
      // Controls
      output stb, we, cyc, stall, lock,
      input stall, lock,
      // Terminators
      input ack, err, rty,
      // Busses
      output adr, dat_m2s,
      input dat_s2m
  );

  modport device(
      // Syscon
      input clk, rst,
      // Controls
      input stb, we, cyc, stall, lock,
      output stall, lock,
      // Terminators
      output ack, err, rty,
      // Busses
      input adr, dat_m2s,
      output dat_s2m
  );
endinterface
