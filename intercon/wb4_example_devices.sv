// Contains various devices and a corresponding partial address decoder
module wb4_example_devices #(
    // parameter int N_CONTROLLERS = 4,
    // parameter int N_DEVICES = 4
) (
    // Controllers
           wishbone_interface.device            wbm,
    // Passthrough device
           wishbone_interface.controller        passthrough,
    // I/O
    output logic                         [32:0] port_a_i,
    output logic                         [32:0] port_a_o,
    output logic                         [32:0] port_b_i,
    output logic                         [32:0] port_b_o,
    output logic                                uart_rx,
    output logic                                uart_tx
);
  // TODO: RAM, ROM, UART, SPI, IO_Ports ..

endmodule
