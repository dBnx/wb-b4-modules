// Quick and dirty and not round robin.
// Currently does not support different amount of masters, then the default.
module arbiter_rr_4 #(
    parameter int N_MASTERS = 4
) (
    // System
    input logic clk,
    input logic rst,
    // Request
    input logic [N_MASTERS-1:0] cyc,
    // Grants
    input logic [N_MASTERS-1:0] gnt
);

  localparam int MastersBits = $clog2(N_MASTERS);
  logic [MastersBits-1:0] counter;

  always_ff @(posedge clk or posedge rst) begin
    if (rst) begin
      counter <= 0;
    end else begin
      if (counter < N_MASTERS) begin
        counter <= counter + 1;
      end else begin
        counter <= 0;
      end
    end
  end

  always_comb begin
    if ((cyc[0] && counter == 0) || cyc == 1 << 0) begin
      gnt = 1 << 0;
    end else if ((cyc[1] && counter == 1) || cyc == 1 << 1) begin
      gnt = 1 << 1;
    end else if ((cyc[2] && counter == 2) || cyc == 1 << 2) begin
      gnt = 1 << 2;
    end else if ((cyc[3] && counter == 3) || cyc == 1 << 3) begin
      gnt = 1 << 3;
    end else begin
      gnt = 0;
    end
  end
endmodule
