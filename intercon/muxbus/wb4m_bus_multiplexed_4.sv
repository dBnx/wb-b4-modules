module wb4m_bus_multiplexed_4 #(
    // parameter int N_CONTROLLERS = 4,
    // parameter int N_DEVICES = 4
) (
    // Controllers
           wishbone_interface.device             wbm_0,
           wishbone_interface.device             wbm_1,
           wishbone_interface.device             wbm_2,
           wishbone_interface.device             wbm_3,
    // Shared device(s)
           wishbone_interface.controller         wb_devices,
    // Arbiter:
    output logic                         [4-1:0] cyc,
    input  logic                         [4-1:0] gnt
);

    assign wb_devices.cyc = wbm_3.cyc ||wbm_2.cyc || wbm_1.cyc || wbm_0.cyc;
    assign cyc = {wbm_3.cyc, wbm_2.cyc, wbm_1.cyc, wbm_0.cyc};

    always_comb begin
        case (gnt)
            'b0001: begin
                wbm_0.ack = wb_devices.ack;
                wbm_0.err = wb_devices.err;
                wbm_0.rty = wb_devices.rty;
                wbm_0.dat_s2m = wb_devices.dat_s2m;

                wb_devices.stb = wbm_0.stb;
                wb_devices.we = wbm_0.we;
                wb_devices.adr = wbm_0.adr;
                wb_devices.sel = wbm_0.sel;
                wb_devices.dat_m2s = wbm_0.dat_m2s;
            end
            'b0010: begin
                wbm_1.ack = wb_devices.ack;
                wbm_1.err = wb_devices.err;
                wbm_1.rty = wb_devices.rty;
                wbm_1.dat_s2m = wb_devices.dat_s2m;

                wb_devices.stb = wbm_1.stb;
                wb_devices.we = wbm_1.we;
                wb_devices.adr = wbm_1.adr;
                wb_devices.sel = wbm_1.sel;
                wb_devices.dat_m2s = wbm_1.dat_m2s;
            end
            'b0100: begin
                wbm_2.ack = wb_devices.ack;
                wbm_2.err = wb_devices.err;
                wbm_2.rty = wb_devices.rty;
                wbm_2.dat_s2m = wb_devices.dat_s2m;

                wb_devices.stb = wbm_2.stb;
                wb_devices.we = wbm_2.we;
                wb_devices.adr = wbm_2.adr;
                wb_devices.sel = wbm_2.sel;
                wb_devices.dat_m2s = wbm_2.dat_m2s;
            end
            'b1000: begin
                wbm_3.ack = wb_devices.ack;
                wbm_3.err = wb_devices.err;
                wbm_3.rty = wb_devices.rty;
                wbm_3.dat_s2m = wb_devices.dat_s2m;

                wb_devices.stb = wbm_3.stb;
                wb_devices.we = wbm_3.we;
                wb_devices.adr = wbm_3.adr;
                wb_devices.sel = wbm_3.sel;
                wb_devices.dat_m2s = wbm_3.dat_m2s;
            end
            default: begin
                wb_devices.stb = 0;
                wb_devices.we = 0;
                wb_devices.adr = 0;
                wb_devices.sel = 0;
                wb_devices.dat_m2s = 0;
            end
        endcase
    end
    assign wb_devices.adr = ;
    // Assign outputs
  assign wb_master.cyc_o = wb_slave.cyc_i;
  assign wb_master.stb_o = wb_slave.stb_i;
  assign wb_master.we_o = wb_slave.we_i;
  assign wb_master.sel_o = 4'b1111; // Assuming full byte enables for simplicity
  assign wb_master.ack_i = wb_slave.ack_o;
  assign wb_master.err_i = 1'b0;

  assign wb_slave.ack_o = wb_master.ack_i;
  assign wb_slave.err_o = 1'b0;

endmodule
